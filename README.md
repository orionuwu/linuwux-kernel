# Linuwux Kernel

My kernel config file based upon the Manjaro default kernel .config

I made this for kernel 6.0.7 since that was the latest verion as of Nov 6 2022, you can just update it to whatever kernel version you're using as detailed in the installation section.

## Installation

Compile the kernel the exact same way one would so for normal linux, that makes sense since this is *just* linux. 

```bash
make oldconfig
make -j$(nproc)
sudo make modules_install
sudo make install
```

## Contributing
I honestly don't know why you'd wanna contribute to this mess, but you can if you want. Just make a pull request and detail out your changes.


## License
[GPL v2](https://choosealicense.com/licenses/gpl-2.0/)
